﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initialize array of product names
            string[] products = new string[5] { "Jabłko", "Baton", "Sok", "Butelka", "Guma" };
            //Create product buyers object array
            Buyer[] buyers = new Buyer[5];
            //Initialize seller object
            Seller seller = new Seller();

            //Define buyers objects and run their constructors with assigned products
            buyers[0] = new Buyer(new string[2] { products[1], products[3] });
            buyers[1] = new Buyer(new string[3] { products[1], products[0], products[2] });
            buyers[2] = new Buyer(new string[2] { products[1], products[2] });
            buyers[3] = new Buyer(new string[2] { products[0], products[3] });
            buyers[4] = new Buyer(new string[4] { products[1], products[3], products[0], products[4] });

            //Iterate through all the buyers, display their products and make them pay for it
            for (int i = 0; i < buyers.Length; i++)
            {
                buyers[i].DisplayProducts();
                buyers[i].BuyProductsFromSeller(seller);
            }
            
            //ReadLine at the end of program
            Console.ReadLine();
        }
    }
}
