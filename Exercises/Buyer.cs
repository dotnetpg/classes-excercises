﻿using System;

namespace Exercises
{
    class Buyer
    {
        //Array of products a single buyer wants to buy
        private string[] _products;

        //Constructor with array of products to assign as parameter
        public Buyer(string[] product)
        {
            _products = product;
        }

        //Iterate through the products and display them on screen
        public void DisplayProducts()
        {
            Console.Write("Products: ");
            for (int i = 0; i < _products.Length; i++)
            {
                Console.Write(_products[i] + ", ");
            }
            Console.WriteLine();
        }

        //Calculate the full product cost and pay for them, with seller as a reference parameter
        public void BuyProductsFromSeller(Seller seller)
        {
            int cost = 0;
            for (int i = 0; i < _products.Length; i++)
            {
                cost += seller.GetCostOfProduct(_products[i]);
            }
            seller.GetRich(cost);
        }
    }
}
