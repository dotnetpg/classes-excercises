﻿using System;
using System.Collections.Generic;

namespace Exercises
{
    class Seller
    {
        //Money earned from selling the products
        private int _money;

        //Constructor with no parameters, only set _money value to 0
        public Seller()
        {
            _money = 0;
        }

        //Return the cost of a single product
        //Ugly solution with switch, for more advanced students: use Dictionary<string, int>
        public int GetCostOfProduct(string product)
        {
            switch(product)
            {
                case "Jabłko":
                    return 50;
                case "Baton":
                    return 150;
                case "Sok":
                    return 400;
                case "Butelka":
                    return 75;
                case "Guma":
                    return 670;
                default:
                    return 0;
            }
        }

        //Receive money from the buyer
        public void GetRich(int money)
        {
            _money += money;
        }

        //The destructor, display the earned money
        ~Seller()
        {
            Console.WriteLine("All products sold! Earned money: " + _money.ToString());
            Console.ReadLine(); //Ugly solution, used only in this case so we can see the effect of destructor
        }
    }
}
